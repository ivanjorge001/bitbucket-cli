const axios = require('axios');
const fs = require('fs');
const path = require('path');

module.exports = ({ program }) => {
	let jsonFilePath = path.join(__dirname, '/../user.json');
	let user = {};

	try {

		let rawdata = fs.readFileSync(jsonFilePath);
		user = JSON.parse(rawdata);
	} catch (error) {
		console.log(error)
	}

	program
	.description('Create a Repo')
	.arguments('<title>')
	.option('-k, --key <key>', 'Key of the project example ABC')
	.option('-u, --username <username>', 'Username to connect to bitbucket')
	.option('-p, --password <password>', 'Password to connect to bitbucket')
	.option('--no-fail', 'Command will not exit with code != 0 even on failure')
	.option('--debug', 'Output message to be sent to Bitbucket API')
	.action((title, cmd) => {
		let username = cmd.username || user.username;
		let password = cmd.password || user.password;

		client = axios.request({
			method: 'POST',
			url: `https://api.bitbucket.org/2.0/repositories/${username}/${title}`,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`
			},
			data: {
				is_private: true,
				scm: "git",
				project: {
					key: cmd.key,
				}
			},
			responseType: 'json'
		}).then((response) => {
			const res = response.data;

			try {
				const { clone } = res.links;
				const url = clone.find((x) => x.name === "https")
				console.log(url.href);

				user.username = username;
				user.password = password;
				fs.writeFile(jsonFilePath, JSON.stringify(user), (error) => {
					if (error) {
						console.log(error);
						console.log('error while saving credentials');
					}
				});

			} catch (error) {
				throw error;
			}
		}).catch((error) => {
			if (error.response) {
				// The request was made and the server responded with a status code
				// that falls out of the range of 2xx
				console.log('static request1', error.response.data);
				console.log('static request2', error.response.status);
				// console.log('static request3', error.response.headers);
			} else if (error.request) {
				// The request was made but no response was received
				// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
				// http.ClientRequest in node.js
				console.log('static request4', error.request);
			} else {
				// Something happened in setting up the request that triggered an Error
				console.log('static request Error5', error.message);
			}
		});

	});
}
